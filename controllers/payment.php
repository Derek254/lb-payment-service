<?php 
    require_once('xit-db.php');
    require_once('mpesa-daraja-api.php');
    class Payment  extends XitDB {

        private $tableNames = array("stkpushqueuedpayments");
        private $debugValue;

        private $mpesaDarApiObj;

        public function __construct($debug = NULL){
            $this->debugValue = $debug;
            parent::__construct($this->debugValue );
            $this->mpesaDarApiObj = new MpesaDarajaAPI();
        }

        public function queuePayment($paymentModel){
            try{
                $xitRes = $this->insert($this->tableNames[0], $paymentModel);
                if($xitRes[0] == 1){
                    return array(1, 'Payment was queued');
                }else{
                    return $xitRes;
                }
            }catch(Exception $e){
                return array(500, $e->getMessage());
            }
        }

        public function expressPayment($paymentModel){

            try{
                $queueRes = $this->queuePayment($paymentModel);
                if($queueRes[0] == 1){
                    $stkReqModel = array("paymentMobileNo"=>$paymentModel["paymentMobileNo"], 
                                        "paymentAmount"=>$paymentModel["paymentAmount"]);
                    $sktRes = json_decode(json_encode($this->mpesaDarApiObj->stkPushRequest($stkReqModel)), true);
                    print_r($sktRes);
                    if(isset($sktRes)){
                        if(isset($sktRes["errorCode"])){
                            return array(0, $sktRes["errorMessage"]);
                        }else{
                            /* Update payment in table */
                            return array(1, $sktRes["ResponseDescription"]);
                        }
                    }else{
                        return array(0, 'There were issues with your request.');
                    }
                }else{
                    return $queueRes;
                }
            }catch(Exception $e){
                return array(500, $e->getMessage());
            }
        }

        public function getQueuedPayments($PaymentType, $keyModel = NULL){
            try{
                $xitRes = $this->fetchArray($this->tableNames[$PaymentType], $keyModel, NULL);
                return $xitRes;
            }catch(Exception $e){
                return array(500, $e->getMessage());
            }
        }

        
    }
?>