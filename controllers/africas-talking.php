<?php 
    require_once('../../xit/libs/africastalking/africas-talking-gateway.php'); 
    class AfricasTalking {

        private $userName = "Skylar";
        private $apiKey = "ba6253ca828e45c615348c2e6d4e864ca11a67e2e99dac7c5f861a5db03b3ec6";

        public function __consructor($debug = NULL){

        }

        /* Single SMS to Many recipients*/
        public function sendSingleToMultiple(){

        }

        /* Many SMSs to Many recipients (NamedPairs)*/
        public function sendMultipleToMultiple($recipientNumber, $messageText){
            try{
                $gatewayObj = new AfricasTalkingGateway($this->userName, $this->apiKey);
                return array(1, $gatewayObj->sendMessage($recipientNumber, $messageText));
            }catch(AfricasTalkingGatewayException $e){
                return array(500, $e->getMessage());
            }
        }
    }
?>