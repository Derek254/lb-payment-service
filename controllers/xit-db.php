<?php
    @require_once('../../shared/db-config.php');
    class XitDB {
        private $debugValue;
        
        public function __construct($debug = NULL){
            $this->debugValue = $debug;
        }

        protected function insert($tableName, $tableModel){
            try{
                global $db;
                $db->debug = $this->debugValue;
                $db->AutoExecute($tableName, $tableModel, 'INSERT');
                if($db->errorMsg() == null){
                    return array(1, 'Inserted');
                }else{
                    return array(0, $db->errorMsg());
                }
            }catch(Exception $e){
                return array(500, $e->getMessage());
            }
        }

        protected function update($tableName, $updateModel, $keyModel){
            try{
                global $db;
                $db->debug = $this->debugValue;
                
            }catch(Exception $e){
                return array(500, $e->getMessage());
            }
        }

        protected function delete($tableName, $keyModel){
            try{
                global $db;
                $db->debug = $this->debugValue;
                
            }catch(Exception $e){
                return array(500, $e->getMessage());
            }
        }

        protected function fetchRow($tableName, $keyModel){
            try{
                global $db;
                $db->debug = $this->debugValue;
                
            }catch(Exception $e){
                return array(500, $e->getMessage());
            }
        }

        protected function fetchArray($tableName, $keyModel = NULL, $likelyHood = NULL){
            try{
                global $db;
                $db->debug = $this->debugValue;
                if($keyModel != NULL){
                    $sqlStatment = "SELECT * FROM " . $tableName . " WHERE ";
                    if(count($keyModel) > 0){
                        $currentItem = 0;
                        foreach($keyModel as $_Key=>$_Value)
                        {
                            $currentItem ++;
                            if($likelyHood != NULL){
                                if($currentItem != count($keyModel) && count($keyModel) != 1){  $sqlStatment .= $_Key . " LIKE '%". $_Value . "%' OR "; }else{  $sqlStatment .= $_Key . " LIKE '%". $_Value . "%' "; }
                            }else{
                                if($currentItem != count($keyModel) && count($keyModel) != 1){  $sqlStatment .= $_Key . " = '". $_Value . "' AND "; }else{  $sqlStatment .= $_Key . " = '". $_Value . "' "; }
                            }
                        }
                    }else {
                        $sqlStatment = "SELECT * FROM " . $tableName;
                    }
                }
                else{
                    $sqlStatment = "SELECT * FROM " . $tableName;
                }
                $resultData = $db->GetArray($sqlStatment);
                if($db->errorMsg() == null){
                    if(count($resultData) > 0){
                        return array(1, 'Found ' . count($resultData) . ' rows.', $resultData);
                    }else{
                        return array(0, 'Found 0 rows.', null);
                    }
                }else{
                    return array(0, $db->errorMsg());
                }
            }catch(Exception $e){
                return array(500, $e->getMessage());
            }
        }

        protected function executeSQL($sqlStatement){
            try{
                global $db;
                $db->debug = $this->debugValue;
            }catch(Exception $e){
                return array(500, $e->getMessage());
            }
        }
    }
?>