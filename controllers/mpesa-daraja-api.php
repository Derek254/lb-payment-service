<?php 
    class MpesaDarajaAPI{
        private $outhURL = "https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";

        private $initiatorName = "Andele Capital Balance";
		private $shortCode = "";
		
		private $stkconsumerKey = "ZwvRy7X8TDz58xkVuwtG7zyZfHn2a5OO";
        private $stkconsumerSecret = "XBlEIOL3oaWYCcIk";
        private $stkshortCode = "683178";
        private $stkpassKey = "052d16f125e8aeb05d09a77f70d7f00de4ca7997ef80b574cbc38b07a3bf33cd";
        private $stkURLRequest = "https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest";
        private $stkURLCallback = "http://payment.lishabora.net/mpesa-callback/stkpush-callback.php";
        
        public function __consructor(){

        }

        public function stkPushRequest($stkReqModel){
            $paymentAmount = $stkReqModel["paymentAmount"];
            $paymentMobileNo = $stkReqModel["paymentMobileNo"];
            $outh = $this->stkconsumerKey . ':' . $this->stkconsumerSecret;

            $curl_outh = curl_init($this->outhURL);
            curl_setopt($curl_outh, CURLOPT_RETURNTRANSFER, 1);

            $credentials = base64_encode($outh);
            curl_setopt($curl_outh, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . $credentials));
            curl_setopt($curl_outh, CURLOPT_HEADER, false);
            curl_setopt($curl_outh, CURLOPT_SSL_VERIFYPEER, false);
            $curl_outh_response = curl_exec($curl_outh);
            $json = json_decode($curl_outh_response, true);

            $time = date("YmdHis", time());
            $Password = $this->stkshortCode . $this->stkpassKey . $time;

            $curl_stk = curl_init();
            curl_setopt($curl_stk, CURLOPT_URL, $this->stkURLRequest);
            curl_setopt($curl_stk, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $json['access_token'])); //setting custom header

            $curl_post_data = array(
                'BusinessShortCode' => $this->stkshortCode,
                'Password' => base64_encode($Password),
                'Timestamp' => $time,
                'TransactionType' => 'CustomerPayBillOnline',
                'Amount' => $paymentAmount,
                'PartyA' => $paymentMobileNo,
                'PartyB' => $this->stkshortCode,
                'PhoneNumber' => $paymentMobileNo,
                'CallBackURL' => $this->stkURLCallback,
                'AccountReference' => '4352',
                'TransactionDesc' => $paymentMobileNo
            );
            $data_string = json_encode($curl_post_data);
            curl_setopt($curl_stk, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_stk, CURLOPT_POST, true);
            curl_setopt($curl_stk, CURLOPT_HEADER, false);
            curl_setopt($curl_stk, CURLOPT_POSTFIELDS, $data_string);

            $curl_stk_response = curl_exec($curl_stk);
            return $curl_stk_response;
        }
    }
?>