<?php 
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../../xit/utils/xit-lib.php';
    include_once '../../xit/utils/xit-dates.php';
    include_once '../../controllers/payment.php';

    $payLoad = json_decode(file_get_contents("http://localhost:8080/LishaBoraSystem/service-payment/xit/schemas/stk-push-request/sample-1.json"), true);
    try{
        $xitDatesObj = new XitDates();
        $initiatorObject = $payLoad["initiatorObject"];
        $paymentObject = $payLoad["paymentObject"];
        $paymentExpressModel = array("initiatorCode"=>$initiatorObject["code"],
                                "initiatorMobile"=>$initiatorObject["mobile"],
                                "initiatorObject"=>json_encode($initiatorObject),
                                "paymentAmount"=>$paymentObject["amount"],
                                "paymentMobileNo"=>$paymentObject["mobile"],
                                "paymentType"=>$paymentObject["type"],
                                "paymentQueuedTime"=>$xitDatesObj->getDateTimeNow(),
                                "paymentSentTime"=>"",
                                "paymentObject"=>json_encode($paymentObject),
                                "paymentStatusCode"=>2,
                                "paymentStatusName"=>"Queued");
       
        $xitLib = new XitLib();
        $paymentObj = new Payment(0);
        echo json_encode($paymentObj->expressPayment($paymentExpressModel));
    }catch(Exception $e){
        echo json_encode(array(0, $e->getMessage()));
    }
    
?>