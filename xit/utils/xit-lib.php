<?php 
    class XitLib {
        private $debugValue;
        public function __construct($debug = NULL){
            
        }

        public function htmlSanitizeModel($inModel){
            foreach($inModel as $keyItem=>$valueItem)
            {
                $cleanString = htmlspecialchars(strip_tags($valueItem));
                $cleanString = preg_replace("/[^a-zA-Z0-9\s]/", "", $cleanString);
                $inModel[$keyItem] = $cleanString;
            }
            return $inModel;
        }

        public function htmlSanitizeString($inString){
            $cleanString = htmlspecialchars(strip_tags($inString));
            $cleanString = preg_replace("/[^a-zA-Z0-9\s]/", "", $cleanString);
            return $cleanString;
        }
    }
?>